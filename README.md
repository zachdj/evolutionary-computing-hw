# evolutionary-computing-hw

Homework assignments for CSCI 6560 - Evolutionary Computing @ The University of Georgia.

## Installation

Dependencies are managed using the [Conda](https://conda.io/docs/user-guide/install/index.html) dependency manager.
First, install Conda and make sure that the `conda` command is available on the system path.

Clone the repository into a local directory.  Then run the following commands:

`cd path/to/my/local/repository`

`conda env create -f environment.yml -n ec`

This will create a virtual environment with all required dependencies.

## Running assignments

1. `cd path/to/my/local/repository`
2. `source activate ec`
3. `python -m hw2`  OR  `python -m hw4`

## Completed Assignments:

- [HW2 - N-Queens and TSP](/assignments/hw2.pdf)
- [HW4 - Escaping Local Minima](/assignments/hw4.pdf)
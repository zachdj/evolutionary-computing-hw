""" Toy problem where the goal is to maximize the value of a binary number

The algorithm used is a simple generational algorithm

Problem formulation:
    - Representation: bitstring
    - Fitness function: decimal value of bitstring (where the leftmost position is most significant)
    - Selection: Tournament selection among 5 randomly-selected individuals
    - Crossover: Uniform crossover
    - Mutation: Random bitflip

"""
import random
from deap import base, creator, tools
from utils.time import WallTimer

random.seed(6560)  # make the runs replicable


def _random_binary_digit():
    return random.randint(0, 1)


def evaluate(individual):
    # returns a tuple that contains the fitness for the given individual
    # in this case, the fitness is just the decimal value represented by the binary string
    decimal_value = 0
    total_digits = len(individual)
    for idx, digit in enumerate(individual):
        power = total_digits - idx - 1
        decimal_value += (2**power) * digit

    # the evaluation function must return a tuple. For single-objective maximization, the tuple has a single element
    return (decimal_value, )


def run(n=5, pop_size=25, cx_prob=0.8, mutation_prob=0.01, max_generations=100, tournsize=5, steady_state=False):
    """ Run the GA for the binary_max problem

    Args:
        n (int): length of the binary number
        pop_size (int): number of individuals in the population
        cx_prob (float): probability of performing crossover at each generation
        mutation_prob (float): probability of performing mutation at each generation
        max_generations (int): stop after this number of generations
        tournsize (int): size of the tournament used in tournament selection
        steady_state (bool): use a steady-state GA instead of a generational GA

    Returns:
        (object, float): best individual, best fitness

    """

    # setup single-optimization fitness maximizer
    creator.create('FitnessMaximizer', base.Fitness, weights=(1.0, ))

    # setup a base class for individuals
    # the base class of an individual is a list, and it has one attribute "fitness"
    creator.create('IndividualBase', list, fitness=creator.FitnessMaximizer)

    # create a toolbox containing utility functions
    toolbox = base.Toolbox()

    """ Register a function to create an individual.
        tools.initRepeat calls creator.IndividualBase() with a single argument - a generator function that
        calls `_random_binary_digit` n times
    """
    toolbox.register('individual', tools.initRepeat, creator.IndividualBase, _random_binary_digit, n)

    """ Register a function to create a population.
        toolbox.population(n) should take a single argument "n".
        tools.initRepeat calls list() with a generator function.  The generator function calls toolbox.individual()
        n times
    """
    toolbox.register('population', tools.initRepeat, list, toolbox.individual)

    # register crossover, mutation, selection, and evaluation operations
    toolbox.register('mate', tools.cxUniform, indpb=1.0)
    toolbox.register('mutate', tools.mutFlipBit, indpb=mutation_prob)
    toolbox.register('select', tools.selTournament, tournsize=tournsize)
    toolbox.register('replace', tools.selWorst)
    toolbox.register('evaluate', evaluate)

    # run the GA

    # create initial population
    population = toolbox.population(n=pop_size)

    # evaluate each individual's fitness
    for individual in population:
        individual.fitness.values = evaluate(individual)

    # run through max_generations
    for generation in range(max_generations):
        # select the individuals for the next generation
        parents = toolbox.select(population, len(population))
        if steady_state:
            parents = toolbox.select(population, 2)

        offspring = list(map(toolbox.clone, parents))

        # apply crossover
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            if random.random() < cx_prob:
                toolbox.mate(child1, child2)  # this modifies the children in place
                del child1.fitness.values
                del child2.fitness.values

        # apply mutation
        for child in offspring:
            toolbox.mutate(child)
            del child.fitness.values

        # evaluate fitness of new individuals:
        invalid_individuals = [indv for indv in offspring if not indv.fitness.valid]
        for individual in invalid_individuals:
            individual.fitness.values = evaluate(individual)

        # update population
        if steady_state:
            # replace the worst members with new offspring
            worst = toolbox.replace(population, k=len(offspring))
            for old, new in zip(worst, offspring):
                idx = population.index(old)
                population[idx] = new

        else:
            # generational algorithm replaces the entire population with the offspring
            population[:] = offspring

    # find the best individual in the final population
    best = max(population, key=evaluate)
    return best, best.fitness.values


def main():
    timer = WallTimer()

    config = {
        'n': 50,
        'pop_size': 25,
        'cx_prob': 0.8,
        'mutation_prob': 0.01,
        'max_generations': 1000,
        'tournsize': 15,
        'steady_state': True
    }
    best, fitness = run(**config)

    print(f'Best individual found: {best}'
          f'\nFitness: {fitness[0]} '
          f'\nTime: {timer} s')


if __name__ == '__main__':
    main()

""" Genetic Algorithm to solve the N-Queens problem

Problem formulation:
    - Representation: permutation
    - Fitness function (To be minimized): Total number of conflicts
    - Parenthood Selection: Tournament selection among 5 randomly selected individuals
    - Crossover: crossover with probability 70%.  If crossover is to be done, then:
        half of the time, do order crossover
        the other half of the time, do PMX
    - Mutation: Mutate an individual with probability 10%.  If a mutation is to be done, then:
        During the first 300,000 iterations, do scramble mutation
        Between 300,000 and 700,000 iterations, do inversion mutation
        After 700,000 iterations, do insert mutation
    - Survival Selection (in the steady-state case)
        Replace a random individual from among the 5 worst individuals

"""

import random
from deap import base, creator, tools
from utils.time import WallTimer


def evaluate(individual):
    """ Computes an individual's fitness

    Returns:
        (int, ): the individual's fitness - the total number of conflicts on the board
    """
    conflicts = 0
    for col1, row1 in enumerate(individual):
        for col2, row2 in enumerate(individual):
            if not (col1 == col2 and row1 == row2):  # don't compare a queen to itself
                dx = col2 - col1
                dy = row2 - row1
                if dx == dy or dx == -dy:
                    conflicts += 1
    fitness = conflicts / 2
    return (fitness, )


def scramble_mutation(individual):
    # scrambles a random portion of the individual, in place
    region_start = random.choice(range(len(individual)))
    region_end = random.choice(range(region_start+1, len(individual)+1))
    scramble_region = individual[region_start:region_end]
    scrambled = random.sample(scramble_region, len(scramble_region))
    individual[region_start:region_end] = scrambled


def inversion_mutation(individual):
    # selects a random portion of the individual, and reverses it in place
    region_start = random.choice(range(len(individual)))
    region_end = random.choice(range(region_start+1, len(individual)+1))
    reversal_region = individual[region_start:region_end]
    reversed_region = reversal_region[::-1]
    individual[region_start:region_end] = reversed_region


def insert_mutation(individual):
    # selects two points in the individual, and moves the second point to be in front of the first
    first_pos = random.choice(range(len(individual)))
    second_pos = random.choice(range(len(individual)))
    element = individual.pop(second_pos)
    individual.insert(first_pos, element)


def init():
    # setup single-optimization fitness minimizer
    creator.create('FitnessMinimizer', base.Fitness, weights=(-1.0,))

    # setup a base class for individuals
    # the base class of an individual is a list, and it has one attribute "fitness"
    creator.create('IndividualBase', list, fitness=creator.FitnessMinimizer)


def run(n=10, pop_size=50, cx_prob=0.8, mutation_prob=0.01, max_iterations=100, tournsize=5, steady_state=True):
    """ Run the GA

        Args:
            n (int): number of queens to place on the chessboard
            pop_size (int): number of individuals in the population
            cx_prob (float): probability of performing crossover
            mutation_prob (float): probability of performing mutation on a newly generated child
            max_iterations (int): stop after this number of fitness evaluations
            tournsize (int): size of the tournament used in tournament selection
            steady_state (bool): use a steady-state GA instead of a generational GA

        Returns:
            (object, int): best individual, number of fitness evaluations done

    """
    # create a toolbox containing utility functions
    toolbox = base.Toolbox()

    # register a function to create random individuals
    toolbox.register('random_permutation', random.sample, range(n), n)
    toolbox.register('individual', tools.initIterate, creator.IndividualBase, toolbox.random_permutation)

    # register a function to create a population
    toolbox.register('population', tools.initRepeat, list, toolbox.individual)

    # register crossover, mutation, selection operations
    toolbox.register('order_crossover', tools.cxOrdered)
    toolbox.register('pmx_crossover', tools.cxPartialyMatched)

    toolbox.register('scramble_mutation', scramble_mutation)
    toolbox.register('inversion_mutation', inversion_mutation)
    toolbox.register('insert_mutation', insert_mutation)

    toolbox.register('select', tools.selTournament, tournsize=tournsize)
    toolbox.register('replace', tools.selWorst)

    # run the GA:
    fitness_evaluations = 0  # keep track of this for termination conditions

    population = toolbox.population(pop_size)
    best_indv = population[0]

    for member in population:
        member.fitness.values = evaluate(member)
        fitness_evaluations += 1
        if member.fitness > best_indv.fitness:
            best_indv = member

    while fitness_evaluations < max_iterations and best_indv.fitness.values[0] != 0:
        # select the individuals to serve as parents
        parents = toolbox.select(population, len(population))
        if steady_state:
            parents = toolbox.select(population, 2)

        offspring = list(map(toolbox.clone, parents))

        # apply crossover
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            if random.random() < cx_prob: # decides whether to do crossover or not
                if random.random() < 0.5: # decides whether to do order or PMX
                    toolbox.order_crossover(child1, child2)
                else:
                    toolbox.pmx_crossover(child1, child2)

                del child1.fitness.values
                del child2.fitness.values

        # apply mutation
        for child in offspring:
            if random.random() < mutation_prob:
                if fitness_evaluations <= 300000:
                    toolbox.scramble_mutation(child)
                elif fitness_evaluations <= 700000:
                    toolbox.inversion_mutation(child)
                else:
                    toolbox.insert_mutation(child)

                del child.fitness.values

        # evaluate fitness of new individuals:
        new_individuals = [indv for indv in offspring if not indv.fitness.valid]
        for individual in new_individuals:
            individual.fitness.values = evaluate(individual)
            fitness_evaluations += 1
            if individual.fitness > best_indv.fitness:
                best_indv = individual

        # update population
        if steady_state:
            # replace two of the worst 5 members
            worst5 = toolbox.replace(population, k=5)
            sample = random.sample(worst5, 2)
            for old, new in zip(sample, offspring):
                idx = population.index(old)
                population[idx] = new
        else:
            # replace the population with the new generation
            population[:] = offspring

    return best_indv, fitness_evaluations


def main(n_values=(6, 8, 12, 21)):
    random.seed(4560)  # make runs replicable
    config = {
        'pop_size': 20,
        'cx_prob': 0.7,
        'mutation_prob': 0.1,
        'max_iterations': 1000000,
        'tournsize': 5,
        'steady_state': True
    }

    init()
    for n in n_values:
        best_indv, fitness_evaluations = run(n=n, **config)
        print(f'Found solution for {n} Queens after {fitness_evaluations} fitness evaluations:\n'
              f'Solution: {best_indv}\n'
              f'Conflicts: {best_indv.fitness.values[0]}')


def grid_search(grid, n_range):
    """ Runs the problem over a discrete grid of hyperparameters """
    random.seed(4560)

    timer = WallTimer()
    param_tuples = [(pop_size, cx_prob, mx_prob, maxit, tournsize, ss)
                    for pop_size in grid['pop_size']
                    for cx_prob in grid['cx_prob']
                    for mx_prob in grid['mutation_prob']
                    for maxit in grid['max_iterations']
                    for tournsize in grid['tournsize']
                    for ss in grid['steady_state']
                    ]

    init()
    num_trials = len(n_range)

    highest_pct_config = (0.0, None)  # the configuration that found solutions for the highest percentage of trials
    highest_success_config = (0, None) # the config that found a solution for the highest value of n
    for settings in param_tuples:
        config = {
            'pop_size': settings[0],
            'cx_prob': settings[1],
            'mutation_prob': settings[2],
            'max_iterations': settings[3],
            'tournsize': settings[4],
            'steady_state': settings[5]
        }
        successes = 0  # for how many values of n did this config find a solution
        max_success = 0  # what was the biggest value of n for which a solution was found

        for n in n_range:
            best_solution, iterations = run(n=n, **config)
            if best_solution.fitness.values[0] == 0:
                successes += 1
                max_success = n

        if max_success > highest_success_config[0]:
            highest_success_config = (max_success, config)

        pct = successes / num_trials
        if pct > highest_pct_config[0]:
            highest_pct_config = (pct, config)

    print(f'Grid search terminated after {timer} seconds\n'
          f'Highest percentage config ({highest_pct_config[0]}): {highest_pct_config[1]} \n'
          f'Highest solution config ({highest_success_config[0]}): {highest_success_config[1]})')


if __name__ == '__main__':
    main(n_values=[22, 23, 24, 25, 26, 27, 28, 29, 30])

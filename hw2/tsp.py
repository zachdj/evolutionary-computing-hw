""" Genetic Algorithm to solve the Travelling Salesman Problem

Problem formulation:
    - Representation: permutation
    - Fitness function (To be minimized): Euclidean distance of the tour
    - Parenthood Selection: Tournament selection among 7 randomly selected individuals
    - Crossover: Edge Recombination with probability 87%
    - Mutation: Mutate an individual with probability 7%.  If a mutation is to be done, then:
        Swap mutation with probability 40%
        Insert mutation with probability 40%
        inversion mutation with probability 20%
    - Survival Selection (in the steady-state case)
        Replace a random individual from among the 5 worst individuals

"""

import random
from deap import base, creator, tools
from utils.time import WallTimer
import utils.distance


def read_data(filepath, starting_line):
    """ Read data for the TSP problem from the file specified by filepath

    Args:
        filepath (str): fully-qualified path to the text file with the TSP data
        starting_line (int): line where the data actually begins

    Returns:
        dict: mapping of city indices to (x, y) coordinates

    """
    with open(filepath, 'r') as file:
        for line in range(starting_line): file.readline()
        lines = file.readlines()

    cities = dict()
    for line in lines:
        components = line.split()
        cities[int(components[0])] = (float(components[1]), float(components[2]))

    return cities


def evaluate(cities, individual):
    """ Fitness function - euclidean distance of the tour represented by the individual

    Args:
        cities: a mapping from city index to (x, y) coords
        individual: a permutation of the city indices

    Returns:
        float: euclidean distance of the tour
    """
    num_cities = len(individual)
    tour_length = 0
    for idx in range(num_cities):
        current_city = individual[idx]
        next_city = individual[(idx+1) % num_cities]
        dist = utils.distance.euclidean(cities[current_city], cities[next_city])
        tour_length += dist
    return (tour_length, )


def edge_recombination(a, b):
    # replaces a and b with offspring generated by the edge recombination operator
    child1 = _edge_recombination(a, b)
    child2 = _edge_recombination(a, b)
    a[:] = child1
    b[:] = child2


def _edge_recombination(a, b):
    # produces an offpsring from a and b using the edge recombination crossover operator

    # build the edge table:
    edge_table = dict()
    num_cities = len(a)
    for city in range(1, num_cities+1):
        edge_table[city] = []

    for idx, city in enumerate(a):
        neighbor1 = a[idx-1]
        neighbor2 = a[(idx+1) % num_cities]
        if neighbor1 not in edge_table[city]:
            edge_table[city].append(neighbor1)
        if neighbor2 not in edge_table[city]:
            edge_table[city].append(neighbor2)

    for idx, city in enumerate(b):
        neighbor1 = b[idx-1]
        neighbor2 = b[(idx+1) % num_cities]
        if neighbor1 not in edge_table[city]:
            edge_table[city].append(neighbor1)
        if neighbor2 not in edge_table[city]:
            edge_table[city].append(neighbor2)

    # pick first element at random
    child = []
    current_element = random.choice(a)
    child.append(current_element)

    while len(child) < num_cities:
        # remove current element from all adjacency lists
        for entry in edge_table:
            try:
                edge_table[entry].remove(current_element)
            except ValueError:
                pass

        neighbors = edge_table[current_element]
        if len(neighbors) == 0:
            # choose randomly among cities not in child
            choices = [x for x in range(1, num_cities+1) if x not in child]
            current_element = random.choice(choices)
        else:
            neighbor_with_fewest_neighbors = min(neighbors, key=lambda x: len(edge_table[x]))
            current_element = neighbor_with_fewest_neighbors

        child.append(current_element)

    return child


def swap_mutation(individual):
    # swaps two positions in the individual
    swap_pts = random.sample(range(len(individual)), 2)
    pt1, pt2 = swap_pts[0], swap_pts[1]
    individual[pt1], individual[pt2] = individual[pt2], individual[pt1]


def inversion_mutation(individual):
    # selects a random portion of the individual, and reverses it in place
    region_start = random.choice(range(len(individual)))
    region_end = random.choice(range(region_start + 1, len(individual) + 1))
    reversal_region = individual[region_start:region_end]
    reversed_region = reversal_region[::-1]
    individual[region_start:region_end] = reversed_region


def insert_mutation(individual):
    # selects two points in the individual, and moves the second point to be in front of the first
    first_pos = random.choice(range(len(individual)))
    second_pos = random.choice(range(len(individual)))
    element = individual.pop(second_pos)
    individual.insert(first_pos, element)


def init():
    # setup single-optimization fitness minimizer
    creator.create('FitnessMinimizer', base.Fitness, weights=(-1.0,))

    # setup a base class for individuals
    # the base class of an individual is a list, and it has one attribute "fitness"
    creator.create('IndividualBase', list, fitness=creator.FitnessMinimizer)


def run(cities, pop_size=50, cx_prob=0.87, mutation_prob=0.07, max_iterations=1000000, tournsize=7, steady_state=True):
    """ Run the GA

        Args:
            n (int): number of queens to place on the chessboard
            pop_size (int): number of individuals in the population
            cx_prob (float): probability of performing crossover
            mutation_prob (float): probability of performing mutation on a newly generated child
            max_iterations (int): stop after this number of fitness evaluations
            tournsize (int): size of the tournament used in tournament selection
            steady_state (bool): use a steady-state GA instead of a generational GA

        Returns:
            (object, int): best individual, number of fitness evaluations done

    """
    # create a toolbox containing utility functions
    toolbox = base.Toolbox()

    # register a function to create random individuals
    num_cities = len(cities)
    toolbox.register('random_permutation', random.sample, range(1, num_cities+1), num_cities)
    toolbox.register('individual', tools.initIterate, creator.IndividualBase, toolbox.random_permutation)

    # register a function to create a population
    toolbox.register('population', tools.initRepeat, list, toolbox.individual)

    # register crossover, mutation, selection operators
    toolbox.register('crossover', edge_recombination)

    toolbox.register('swap_mutation', swap_mutation)
    toolbox.register('inversion_mutation', inversion_mutation)
    toolbox.register('insert_mutation', insert_mutation)

    toolbox.register('select', tools.selTournament, tournsize=tournsize)
    toolbox.register('replace', tools.selWorst)

    toolbox.register('evaluate', evaluate, cities)

    # run the GA:
    fitness_evaluations = 0  # keep track of this for termination conditions

    population = toolbox.population(pop_size)
    best_indv = population[0]

    for member in population:
        member.fitness.values = toolbox.evaluate(member)
        fitness_evaluations += 1
        if member.fitness > best_indv.fitness:
            best_indv = member

    while fitness_evaluations < max_iterations and best_indv.fitness.values[0] != 118282:
        # select the individuals to serve as parents
        parents = toolbox.select(population, len(population))
        if steady_state:
            parents = toolbox.select(population, 2)

        offspring = list(map(toolbox.clone, parents))

        # apply crossover
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            if random.random() < cx_prob:  # decides whether to do crossover or not
                toolbox.crossover(child1, child2)
                del child1.fitness.values
                del child2.fitness.values

        # apply mutation
        for child in offspring:
            if random.random() < mutation_prob:
                decider = random.random()
                if decider < 0.4:
                    toolbox.swap_mutation(child)
                elif decider < 0.8:
                    toolbox.insert_mutation(child)
                else:
                    toolbox.inversion_mutation(child)

                del child.fitness.values

        # evaluate fitness of new individuals:
        new_individuals = [indv for indv in offspring if not indv.fitness.valid]
        for individual in new_individuals:
            individual.fitness.values = toolbox.evaluate(individual)
            fitness_evaluations += 1
            if individual.fitness > best_indv.fitness:
                best_indv = individual

        # update population
        if steady_state:
            # replace two of the worst 5 members
            worst5 = toolbox.replace(population, k=5)
            sample = random.sample(worst5, 2)
            for old, new in zip(sample, offspring):
                idx = population.index(old)
                population[idx] = new
        else:
            # replace the population with the new generation
            population[:] = offspring

    return best_indv, fitness_evaluations


def main():
    random.seed(6560)
    timer = WallTimer()
    data_file = 'data/tsp-data.txt'
    config = {
        'cities': read_data(data_file, 2),
        'pop_size': 50,
        'cx_prob': 0.87,
        'mutation_prob': 0.07,
        'max_iterations': 1000000,
        'tournsize': 7,
        'steady_state': True
    }
    init()
    best_solution, iterations = run(**config)

    print(f'Travelling Salesman Problem:\n'
          f'Terminated after {iterations} iterations\n'
          f'Best solution found: {best_solution}\n'
          f'Length of the tour: {best_solution.fitness.values[0]}\n'
          f'Time: {timer} s\n\n')


if __name__ == '__main__':
    main()

""" Genetic algorithm for minimization of an n-dimensional continuous function

Formulation:
    Representation: vector of floating point numbers
    Parenthood Selection: tournament selection of size k (configurable)
    Mutation: non-uniform mutation
     X' = <x_1, ... x_i', ... x_n>
         where x_i' = x_i + delta(t, right(i) - x_i)  50% of the time
         and x_i' = x_i - delta(t, x_i - left(i))  the other 50% of the time

         using the annealing function delta(t, y) = y * r * (1 - t/T)**b
         where r is a random number between [0, 1], T is the maximum number of iterations, and b=1.5

    Crossover: whole arithmetic crossover with alpha randomly chosen between [0, 1.2]
    Survival selection: replace a random member from the bottom 20% of the population
 """

import random
import numpy as np
import math
from utils.time import WallTimer


class OptimizingGA:
    def __init__(self, dimensions, fitness_fn,
                 minimize=True,
                 max_evaluations=2000,
                 pop_size=20,
                 cx_prob=0.9,
                 mt_prob=0.05,
                 tourn_size=5,
                 steady_state=True,
                 known_optimum=None):

        self.dimensions = dimensions
        self.fitness_fn = fitness_fn
        self.minimize = minimize
        self.pop_size = pop_size
        self.bottom_twenty = int(math.floor(0.2*self.pop_size))  # number of individuals in the bottom 20%
        self.cx_prob = cx_prob
        self.mt_prob = mt_prob
        self.tourn_size = tourn_size
        self.steady_state = steady_state
        self.known_optimum = known_optimum

        # variables that track progress
        self.total_evaluations = 0
        self.max_evaluations = max_evaluations
        self.best_indv = None

        # initialize population of random individuals
        self.population = [Individual(dimensions) for i in range(0, pop_size)]
        self.best_indv = self.population[0]
        for indv in self.population:
            self._evaluate(indv)
            if self._compare(indv, self.best_indv) > 0:
                self.best_indv = indv

    def run(self):
        """ Runs the GA until the maximum number of iterations is achieved

        Returns: time taken during the run (in seconds)
        """
        timer = WallTimer()
        steady_state = self.steady_state
        while self.total_evaluations < self.max_evaluations and self.best_indv.fitness != self.known_optimum:
            # select parents
            if steady_state:
                parents = self._select(2)
            else:
                parents = self._select(self.pop_size)

            # make children using crossover
            offspring = list()
            for parent1, parent2 in zip(parents[::2], parents[1::2]):
                if random.random() < self.cx_prob:
                    child1, child2 = self._crossover(parent1, parent2)
                    offspring.append(child1)
                    offspring.append(child2)

            # mutate
            for child in offspring:
                if random.random() < self.mt_prob:
                    self._mutate(child)

                # even if the child wasn't mutated, his fitness needs to be re-evaluated after crossover
                self._repair(child)
                self._evaluate(child)
                if self._compare(child, self.best_indv) > 0:
                    self.best_indv = child

            # update population
            for child in offspring:
                self.population.sort(key=lambda indv: -indv.fitness if self.minimize else indv.fitness)
                replacement_index = random.choice(range(0, self.bottom_twenty))
                self.population[replacement_index] = child

        return timer.since()

    def _select(self, n):
        """ Selects n individuals by running n tournaments """
        selected = list()
        while len(selected) < n:
            # run a tournament of size `self.tourn_size`
            competitors = random.sample(self.population, self.tourn_size)
            winner = max(competitors, key=lambda indv: -indv.fitness if self.minimize else indv.fitness)
            selected.append(winner)

        return selected

    def _crossover(self, parent1, parent2):
        """ Generate two children using the whole arithmetic crossover of parent1 and parent2

        Args:
            parent1 (Individual): the first parent
            parent2 (Individual): the second parent

        Returns: (Individual, Individual): tuple of children created by the crossover
        """
        alpha = random.random() * 1.2  # alpha randomly chosen between [0, 1.2]
        child1, child2 = parent1.clone(), parent2.clone()
        child1.vector = alpha*parent1.vector + (1-alpha)*parent2.vector
        child2.vector = (1-alpha)*parent1.vector + alpha*parent2.vector
        child1.fitness, child2.fitness = None, None
        return child1, child2

    def _mutate(self, indv):
        """ Performs non-uniform mutation on an individual in-place

        Args:
            indv (Individual): the individual to mutate

        Returns: None
        """
        position = random.choice(range(0, len(self.dimensions)))  # choose a position to mutate
        left, right = self.dimensions[position][0], self.dimensions[position][1]
        old_val = indv.vector[position]
        if random.random() < 0.5:
            y = right - old_val
            new_val = old_val + y * random.random() * (1 - self.total_evaluations/self.max_evaluations)**1.5
        else:
            y = old_val - left
            new_val = old_val - y * random.random() * (1 - self.total_evaluations/self.max_evaluations)**1.5

        indv.vector[position] = new_val

    def _repair(self, indv):
        """ Fixes any gene that is outside of its allowed range

        This can sometimes happen with whole arithmetic crossover since we allow alpha > 1

        Args:
            indv (Individual): the individual to repair

        Returns: None
        """
        for index in range(0, len(indv)):
            lower_b, upper_b = self.dimensions[index][0], self.dimensions[index][1]
            value = indv.vector[index]
            indv.vector[index] = min(upper_b, max(value, lower_b))  # clip value into upper and lower bounds

    def _evaluate(self, indv):
        """ Evaluates the given individual using this GA's fitness function

        Args:
            indv (Individual): the individual to evaluate

        Returns: None
        """
        indv.evaluate(self.fitness_fn)
        self.total_evaluations += 1

    def _compare(self, indv1, indv2):
        """ Compares two individuals
        Args:
            indv1 (Individual): individual A
            indv2 (Individual): individual B

        Returns (int):
            1 if A has fitness better than B
            0 if A has fitness equivalent to B
            -1 if A has fitness worse than B

        """
        sign = 1
        if self.minimize:
            sign = -1

        if sign*indv1.fitness > sign*indv2.fitness:
            return 1
        elif indv1.fitness == indv2.fitness:
            return 0
        else:
            return -1


class Individual:
    def __init__(self, dimensions):
        self.dimensions = dimensions
        self.vector = np.zeros(len(dimensions))
        for index, bounds in enumerate(dimensions):
            lower_b, upper_b = bounds[0], bounds[1]
            self.vector[index] = random.uniform(lower_b, upper_b)
        self.fitness = None

    def evaluate(self, fitness_fn):
        self.fitness = fitness_fn(self.vector)

    def clone(self):
        cloned_self = Individual(self.dimensions)
        cloned_self.vector = np.copy(self.vector)
        cloned_self.fitness = self.fitness
        return cloned_self

    def __str__(self):
        return f'Vector: {self.vector}\tFitness: {self.fitness}'

    def __len__(self):
        return len(self.vector)

import logging, sys
import datetime
import math, random
from hw4.genetic_algorithm import OptimizingGA

def main():
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(message)s')
    # logging.basicConfig(filename=f'results/problem2-{datetime.datetime.now()}.log', level=logging.INFO, format='%(message)s')
    logging.info('HOMEWORK 4, PROBLEM 2')

    def ackley_30(vector):
        """ 30-dimensional Ackley Function - https://en.wikipedia.org/wiki/Ackley_function """
        first_sum, second_sum = 0, 0
        for i in range(0, 30):
            first_sum += vector[i]**2
            second_sum += math.cos( 2 * math.pi * vector[i])
        first_sum *= 1/30
        second_sum *= 1/30

        return -20 * math.exp(-0.2 * first_sum**0.5) - math.exp(second_sum) + 20 + math.e

    DIMENSIONS = [[-30, 30] for i in range(0, 30)]

    # setup a GA to optimize the ackley fitness function
    POP_SIZE = 60
    config = {
        'dimensions': DIMENSIONS,
        'fitness_fn': ackley_30,
        'minimize': True,
        'max_evaluations': 200000,
        'pop_size': POP_SIZE,
        'cx_prob': 1.0,
        'mt_prob': 0.5,
        'tourn_size': POP_SIZE // 4,
        'steady_state': True,
        'known_optimum': 0
    }

    logging.info(f'CONFIG: {config}')

    NUM_TRIALS = 10
    sum_fitness = 0
    for trial in range(1, NUM_TRIALS+1):
        random.seed(trial)
        ga = OptimizingGA(**config)
        time = ga.run()
        logging.info(f'\nTRIAL #{trial}:')
        logging.info(f'Time to run: {time}s')
        logging.info(f'Best solution found:\n{ga.best_indv}')
        sum_fitness += ga.best_indv.fitness

    logging.info(f'\n\nSUMMARY: Problem 2: Average fitness: {sum_fitness/NUM_TRIALS}')


if __name__ == '__main__':
    main()

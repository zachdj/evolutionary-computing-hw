import logging, sys
import datetime
import math, random
from hw4.genetic_algorithm import OptimizingGA


def main():
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(message)s')
    # logging.basicConfig(filename=f'results/problem1-{datetime.datetime.now()}.log', level=logging.INFO, format='%(message)s')
    logging.info('HOMEWORK 4, PROBLEM 1a')

    def ez_fitness_function(vector):
        x, y = vector[0], vector[1]
        component1 = math.fabs(x) + math.fabs(y)
        component2 = math.fabs(
            math.sin(
                math.fabs(x) * math.pi
            )
        )
        component3 = math.fabs(
            math.sin(
                math.fabs(y) * math.pi
            )
        )
        return component1 * (1 + component2 + component3)

    # setup a GA to optimize the easy fitness function
    POP_SIZE = 12
    config = {
        'dimensions': [
            [-60, 40],
            [-30, 70]
        ],
        'fitness_fn': ez_fitness_function,
        'minimize': True,
        'max_evaluations': 2000,
        'pop_size': POP_SIZE,
        'cx_prob': 1.0,
        'mt_prob': 0.8,
        'tourn_size': POP_SIZE // 4,
        'steady_state': True,
        'known_optimum': 0
    }

    logging.info(f'CONFIG: {config}')

    NUM_TRIALS = 10
    sum_fitness = 0
    for trial in range(1, NUM_TRIALS+1):
        random.seed(trial)
        ga = OptimizingGA(**config)
        time = ga.run()
        logging.info(f'\nTRIAL #{trial}:')
        logging.info(f'Time to run: {time}s')
        logging.info(f'Best solution found:\n{ga.best_indv}')
        sum_fitness += ga.best_indv.fitness

    logging.info(f'\n\nSUMMARY: Problem 1a: Average fitness: {sum_fitness/NUM_TRIALS}')

    # start problem 1b
    logging.info('\n\nHOMEWORK 4, PROBLEM 1b')

    def hard_af_fitness_function(vector):
        x, y = vector[0], vector[1]
        component1 = math.fabs(x) + math.fabs(y)
        component2 = math.fabs(
            math.sin(
                3 * math.fabs(x) * math.pi
            )
        )
        component3 = math.fabs(
            math.sin(
                3 * math.fabs(y) * math.pi
            )
        )
        return component1 * (1 + component2 + component3)

    config['fitness_fn'] = hard_af_fitness_function

    sum_fitness = 0
    for trial in range(1, NUM_TRIALS+1):
        random.seed(trial)
        ga = OptimizingGA(**config)
        time = ga.run()
        logging.info(f'\nTRIAL #{trial}:')
        logging.info(f'Time to run: {time}s')
        logging.info(f'Best solution found:\n{ga.best_indv}')
        sum_fitness += ga.best_indv.fitness

    logging.info(f'\n\nSUMMARY: Problem 1b: Average fitness: {sum_fitness/NUM_TRIALS}')


if __name__ == '__main__':
    main()

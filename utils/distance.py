""" Utility functions to compute distance metrics

"""

import math


def hamming(a, b):
    """ Computes the Hamming distance between sequence a and sequence b

    Args:
        a (iterable): the first sequence
        b (iterable): the second sequence

    Returns:
        (int): the Hamming distance between a and b

    """
    if len(a) != len(b):
        raise ValueError('Hamming distance is undefined for sequences of unequal length')

    return sum([v1 != v2 for v1, v2 in zip(a, b)])


def euclidean(a, b):
    """ Computes the Euclidean distances between sequence a and sequence b

    a and b must be sequences of numeric types

    Args:
        a (iterable): the first sequence
        b (iterable): the second sequence

    Returns:
        float: Euclidean distance between a and b

    """
    sum_squared_dist = 0
    for x0, x1 in zip(a, b):
        sum_squared_dist += (x1-x0)**2

    return math.sqrt(sum_squared_dist)


def __test__():
    assert(hamming([1, 1, 1], [1, 1, 1]) == 0)
    assert(hamming([0, 1, 1], [1, 1, 1]) == 1)
    assert(hamming([0, 1, 0], [1, 1, 1]) == 2)
    assert(hamming([0, 0, 0], [1, 1, 1]) == 3)
    assert(hamming("aaba", "bbab") == 4)

    assert(euclidean([1, 1], [0, 0]) == math.sqrt(2))
    assert(euclidean([1, 1], [-1, -1]) == math.sqrt(8))


if __name__ == '__main__':
    __test__()